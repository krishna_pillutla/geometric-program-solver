#PrimalDual <- function(n, m, A, coeff, lstA, lstC, x0)

A <- matrix(c(1, 0, 1, 0.5), nrow = 2)
coeff = c(1, 2)

A1 <- matrix(c(-1, -2, 1, 0), nrow = 2)
c1 <- c(1,1)

A2 <- matrix(c(-1, 0, 0, -1), nrow = 2)
c2 <- c(1, 1)

lstA <- list(A1, A2)
lstC <- list(c1, c2)

#PrimalDual(2, 1, A, coeff, lstA, lstC, c(log(2), 0))
SolveGeometricProgram(2, 1, A, coeff, lstA, lstC, c(2, 1))