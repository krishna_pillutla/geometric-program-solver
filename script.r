library("Matrix")
funcVal <- function(A, coeffs, x){
	# sum of c_i exp (a_i ' x)
	aTx <- A %*% x
	eaTx <- sapply(aTx, exp)
	val <- coeffs %*% eaTx
	return (log(as.vector(val)))
}
	

funcGrad <- function (A, coeffs, x){
	aTx <- A %*% x
	eaTx <- sapply(aTx, exp)
	val <- as.vector(coeffs %*% eaTx)
	mult <- coeffs * eaTx
	vec <- t(A) %*% mult
	ret<- (vec / val)
	return (ret)
} 

funcHess <- function (A, coeffs, x){
	aTx <- A %*% x
	eaTx <- sapply(aTx, exp)
	val <- as.vector(coeffs %*% eaTx)
	mult <- coeffs * eaTx
	vec <- t(A) %*% mult
	grad <- (vec / val)
	
	m1 <- mult[1] * (A[1,] %o% A[1,]) 
	if (nrow(A) >=2){
		for (i in 2:nrow(A)){
			m1 <- m1 + mult[i] * (A[i,] %o% A[i,]) 
		}
	}
	m1 <- m1 / val
	m2 <- (grad %o% grad) [,,,]
	return (m1-m2)
}

ComputeFuncs <- function(m, lstA, lstC, x)
{
	f <- rep(0, m)
	for (i in 1:m){
		f[i]  <- funcVal(lstA[[i]], lstC[[i]], x)
	}
	return (f)
}

ComputeGrads <- function(m, lstA, lstC, x)
{
	f1 <- t(funcGrad(lstA[[1]], lstC[[1]], x))
	if (m >= 2){
		for (i in 2:m){
			f1 <- rbind(f1, t(funcGrad(lstA[[i]], lstC[[i]], x)))
		}
	}
	return (f1)
}

ComputeStep <- function(m, A, coeff, lstA, lstC, x, la, t_param)
{
	EPS <- 1e-06
	perturbation <- 1e-04
	
	fvector <- ComputeFuncs(m, lstA, lstC, x)
	
	m11 <- funcHess(A, coeff, x)
	for (i in 1:m){
		m11 <- m11 + la[i] * funcHess(lstA[[i]], lstC[[i]], x)	
	}
	Df <- ComputeGrads(m, lstA, lstC, x)
	m12 <- t(Df)
	m1 <- cbind(m11, m12)
	
	diaglambda <- if (m == 1)  (-1 * la * diag(1)) else -1 * diag(la)
	
	m21 <- diaglambda %*% Df
	
	fmat <- if (m == 1) fvector * diag(1) else diag(fvector)
	
	m22 <- -1 * fmat
	m2 <- cbind(m21, m22)
	
	mat <- rbind (m1, m2)
	
	rcent <- -1 * la * fvector - (1/t_param)
	
	rdual <- funcGrad(A, coeff, x)
	rdual <- rdual + t(Df) %*% la
	
	rhs <- rbind(rdual, rcent)
	rhs <- -1 * rhs
	
	n <- length(x)
	while (det(mat) < EPS){
		mat <- mat + diag(perturbation, (m+n))
		perturbation <- 10*perturbation
	}
	
	matinv <- solve(mat)
	
	
	return (matinv %*% rhs)
	
	
}

PrimalDual <- function(n, m, A, coeff, lstA, lstC, x0, EPS_OUTER = 1e-06)
{
	## n is number of primal variables
	## m is number of dual variables
	## lstA is a list of A matrices, one for each m constraints
	## lstC is a list of c vectors, one for each m constraints
	
	## la mx1 is the dual
		
	MU <- 10
	MAXITER = 100
	#EPS_OUTER <- 1e-08

	##Initialization
	la <- rep(0, m)
	for (i in 1:m){
		la[i] <- -1 / funcVal(lstA[[i]], lstC[[i]], x0)
		# positive surrogate duality gap
	}
	x <- x0
	
	print("Initial point is ")
	print(x)
	print("Dual variable is")
	print(la)
	print("Function value is" )
	print(funcVal(A, coeff, x))
	print("Constraints are")
	print(ComputeFuncs(m, lstA, lstC, x))
	
	for (iii in 1:MAXITER){
		print(c("iteration", iii))
		#STEP 1:
		Gap <- -1 * (la %*% ComputeFuncs(m, lstA, lstC, x))
		t_param = MU * m / Gap
		print(c(Gap, t_param))
		if (Gap < EPS_OUTER){
			print(c("Algorithm Converged in iteration ", iii))
			break;
		}
		if (Gap < 0){
			print("ERROR")
			break	
		}
		
		## STEP 2:
		NewtonStep <- ComputeStep(m, A, coeff, lstA, lstC, x, la, t_param)
		NewtonStep <- 0.1 * NewtonStep
		
		## STEP 3: update
		x <- x + NewtonStep[1:n]
		la <- la + NewtonStep[(n+1):(n+m)]
		
		print("Primal point is ")
	print(x)
	print("Dual variable is")
	print(la)
	print("Function value is" )
	print(funcVal(A, coeff, x))
	print("Constraints are")
	print(ComputeFuncs(m, lstA, lstC, x))
		
	}
	print("Optimal point is ")
	print(x)
	print("Dual variable is")
	print(la)
	print("Function value is" )
	print(funcVal(A, coeff, x))
	print("Constraints are")
	print(ComputeFuncs(m, lstA, lstC, x))
	return (x)
}

SolveGeometricProgram <- function(n, m, A, coeff, lstA, lstC, x0, EPS_OUTER = 1e-04)
{
	y0 <- log(x0)
	y <- PrimalDual(n,m, A, coeff, lstA, lstC, y0, EPS_OUTER)
	x <- exp(y)
	return (x)
}


